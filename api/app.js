const express = require('express'),
      app = express(),
      morgan = require('morgan'),
      bodyParser = require('body-parser'),
      productRoutes = require('./routes/products'),
      orderRoutes = require('./routes/orders'),
      userRoutes = require('./routes/users'),
      mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/andrey');
mongoose.Promise = global.Promise;

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'))
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content-Type, Accept, Authorization');
   if(req.method === "OPTIONS") {
      res.header('Access-Control-Allow-Origin', "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
   }
   next();
})

app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/user', userRoutes);

app.use((req, res, next) => {
   const error = new Error('Not found');
   error.status = 404;
   next(error);
});
app.use((error, req, res, next) => {
   res.status(error.status || 500);
   res.json({
      error: {
         message: error.message
      }
   })
})

module.exports = app;