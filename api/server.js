const http = require('http'),
      app = require('./app'),
      port = process.env.PORT || 8000,
      server = http.createServer(app);
      
server.listen(port, () => {
   console.log("Server run on", port);
});    
    