const express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'),
      Order = require('./../models/order'),
      checkAuth = require("../middleware/check-auth")
      Product = require('./../models/product'),
      OrdersController = require("../controllers/orders");

router.get('/', checkAuth, OrdersController.orders_get_all);
router.post('/', checkAuth, (req, res, next) => {
	Product.findById(req.body.productId)
		.then(product => {
         if(!product) {
            return res.status(404).json({
               message: "Product not found"
            })
         }
			const order = new Order({
				_id: mongoose.Types.ObjectId(),
				quantity: req.body.quantity,
				product: req.body.productId
			});
			return order.save()
      })
      .then(result => {
         console.log(result);
         res.status(201).json({
            message: 'Order stored',
            createOrder: {
               _id: result._id,
               product: result.product,
               quantity: result.quantity
            },
            request: {
               type: 'GET',
               url: 'http://localhost:8000/api/orders/' + result._id
            }
         })
      })
      .catch(err => {
         console.log(err);
         res.status(500).json({
            error: err
         });
      })
});

router.get('/:id', checkAuth, (req, res, next) => {
   Order.findById(req.params.id)
      .exec()
      .then(order => {
         if(!order) {
            return res.status(404).json({
               message: "Order not found"
            })
         }
         res.status(200).json({
            order: order,
         })
      })
      .catch(err => {
         error: err
      })
});
router.patch('/:id', checkAuth,  (req, res, next) => {
	res.status(200).json({
		message: "Handling PATCH request to /api/products/:id ",
		id: req.params.id
	})
});
router.delete('/:id', checkAuth, (req, res, next) => {
   Order.remove({_id: req.params.id})
      .exec()
      .then(order => {
         res.status(200).json({
            message: "Order deleted"
         })
      })
      .catch(err => {
         error: err
      })
});

module.exports = router;