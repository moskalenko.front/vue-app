const express = require("express"),
      router = express.Router(),
      Product = require('./../models/product'),
      mongoose = require('mongoose'),
      multer = require('multer'),
      checkAuth = require('../middleware/check-auth')
      storage = multer.diskStorage({
         destination: function(req, file, cb) {
            cb(null, './uploads/');
         },
         filename: function(req, file, cb) {
            const now = new Date().toISOString(),
                  date = now.replace(/:/g, '-');

            cb(null, date + file.originalname);
         }
      }),
      fileFilter = (req, file, cb) => {
         if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
            cb(null, true);
         } else {
            cb(null, false);
         }
      }
      upload = multer({
         storage: storage, 
         limits:{
            fileSize: 1024 * 1024 * 5
         },
         fileFilter: fileFilter
      });

router.get("/", (req, res, next) => {
   Product.find()
      .exec()
      .then(docs => {
         const response = {
            count: docs.length,
            products: docs.map(doc => {
               return {
                  name: doc.name,
                  price: doc.price,
                  _id: doc._id,
                  productImage: doc.productImage,
                  request: {
                     type: 'GET',
                     url: "http://localhost:8000/api/products/" + doc._id
                  }
               }
            })
         }
         res.status(200).json(response);
      })
      .catch(err => {
         console.log(err);
         res.status(500).json({
         error: err
         })
      });
});
router.post("/", checkAuth, upload.single('productImage'), (req, res, next) => {
   console.log(req.file);
   const product = new Product({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      price: req.body.price,
      productImage: req.file.path
   });

   product
      .save()
      .then( result => {
         res.status(201).json({
            message: 'Created product successfully',
            createdProduct: {
               name: result.name,
               price: result.price,
               _id: result._id,
            }
         });
      })
      .catch( err => { 
         console.log(err); 
         res.status(500).json({
         error: err
         });
      });
});

router.get("/:id", (req, res, next) => {
   const id = req.params.id;

   Product.findById(id)
      .exec()
      .then(doc => { 
         if(doc) {
            res.status(200).json(doc);
         } else {
            res.status(404).json({
               message: "No valid found"
            })
         }
      })
      .catch( err => {
         console.log(err)
         res.status(500).json({error: err})
      });
});
router.patch("/:id", checkAuth, (req, res, next) => {
  const id = req.params.id;

   Product.update({_id: id}, { $set: req.body})
      .exec()
      .then( result => {
         res.status(200).json({
            message: "Product updated"
         });
      })
      .catch( err => {
            res.status(500).json({
            message: err
         })
      })
});
router.delete("/:id", checkAuth, (req, res, next) => {
   const id = req.params.id;
   Product.remove({ _id: id })
      .exec()  
      .then( result => {
            res.status(200).json({
            message: "Product deleted"
         });
      })
      .catch( err => {
            console.log(err);
            res.status(500).json({
            error: err
         })
      })
});

module.exports = router;
