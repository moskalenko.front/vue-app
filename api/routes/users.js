const express = require("express"),
      router = express.Router(),
      mongoose = require('mongoose'),
      bcrypt = require('bcrypt'),
      User = require('./../models/user'),
      jwt = require('jsonwebtoken');

router.post('/signup', (req, res, next) => {
   User.find({email: req.body.email})
      .exec()
      .then( user => {
         if(user.length >= 1) {
            return res.status(409).json({
               message: 'Mail exists'
            })
         } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
               if(err) {
                  return res.status(500).json({
                     error: err
                  })
               } else {
                  const user = new User({
                     _id: new mongoose.Types.ObjectId(),
                     email: req.body.email,
                     password: hash
                  });  
                  user
                     .save()
                     .then( result => {
                        console.log(result);
                        res.status(201).json({
                           message: 'User created'
                        });
                     })
                     .catch( err => {
                        console.log(err);
                        res.status(500).json({
                           error: err
                        })
                     });  
               }
            })
         }
      })
      .catch();
})

router.post('/login', (req, res, next) => {
   User.find({email: req.body.email})
      .exec()
      .then(user => {
         if(user.length < 1) {
            return res.status(401).json({
               message: 'Auth failed'
            });
         };
         bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            console.log(result);
            if(err){
               return res.status(401).json({
                  message: 'Auth failed'
               });
            };
            if(result) {
               const token = jwt.sign({
                  email: user[0].email,
                  userId: user[0]._id
               }, 
               process.env.JWT_KEY,
               {
                  expiresIn: "1h"
               });
               return res.status(200).json({
                  message: "Auth seccessful",
                  token: token
               });
            };
            res.status(401).json({
               message: 'Auth failed'
            });
         });
      })
      .catch(err => {
         console.log(err);
         res.status(500).json({
            error: err
         });
      });
})

router.delete('/:id', (req, res, next) => {
   User
      .remove({_id: req.params.id})
      .exec()
      .then(result => {
         res.status(200).json({
            message: "User deleted"
         })
      })
      .catch(err => {
         console.log(err);
         res.status(500).json({
            error: err
         });
      })
})

module.exports = router;